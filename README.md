# Base ECS Repo

A bunch of defaults that I like using in my projects for ECS.

## Requirements

- Unity 2018.3.0b2 Personal
- Git
- VSCode

## Content

- Burst
- Collections
- Entitites
- Input System
- Jobs
- Mathematics
- Bootstrap Script
- Sample System

## How to use

- Most code in this repo are samples. You can just delete them from the Samples Folder.
- If you want to try the samples, they should be self-contained, follow these steps:
  - Open the Entity Debugger
  - Click Show Inactive Systems
  - Turn on one of the Sample Systems (they will spawn all the needed entities on enable, and clear on disable)
  - While it is possible to turn on multiple samples at the same time, beware, side effects and weird stuff will happen
  So avoid doing so unless you know what you are doing.

## Available samples

### Simple

Very basic system with the highest level construct I can think of. This is what 99% of your systems should be like. Push
a cube in the Z direction.

### Custom

Very basic system with a lower level construct. This is useful for when you need more granular control. Try to avoid
doing this often. Is just annoying to maintain.

### Spawner

A sample with the steps for spawning entities per frame and removing entities in bulk. Tweak it a bit and it can do the
inverse.

### ParentTransform

Dealing with Parent - Child transform seems dauting at first. But here is the tiniest amount of code to make it work.
All Position, Rotation, and Scale changes of the child are relative to the Matrix of the parent. The parent on the other
hand is relative to the world, so it technically has no dependency.

### GroupedPositionSystem

Very simple movement system, grouped with the next system, and triggering at a fixed interval due to the attribute
constraining the group to run after FixedUpdate.

### GroupedRotationSystem

Very simple rotation system, grouped with the previous system, and triggering at a fixed interval due to the attribute
constraining the group to run after FixedUpdate.
