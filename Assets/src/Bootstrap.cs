﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bootstrap {
    #region Unity Hooks
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Initialize() => SceneManager.sceneLoaded += InitializeWithScene;
    private static void InitializeWithScene(Scene scene, LoadSceneMode loadSceneMode) => NewGame();
    #endregion

    private static void NewGame() {
        var entityManager = World.Active.GetOrCreateManager<EntityManager>();

        /* Initialize all the entities here. Or use spawner systems. */
    }

    #region Helpers

    /// <summary>
    /// Given a name, extract a MeshInstanceRendered
    /// </summary>
    /// <param name="protoName"><see cref="string"/></param>
    /// <returns><see cref="MeshInstanceRenderer"/></returns>
    private static MeshInstanceRenderer GetLookFromPrototype(string protoName) {
        var proto = GameObject.Find(protoName);
        var mirc = proto.GetComponent<MeshInstanceRendererComponent>().Value;
        Object.Destroy(proto);
        return mirc;
    }

    #endregion
}
