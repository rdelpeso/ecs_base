using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class SampleSystem : JobComponentSystem {
  private EntityManager em;
  private List<BaseSampleSystem> systems;
  private List<bool> states;

  /* Disabled by default */
  protected override void OnCreateManager() {
    Debug.Log("Starting sample system... Disable at src/Samples/SampleSyste.cs:19");
    Enabled = true;

    em = World.Active.GetExistingManager<EntityManager>();
    systems = new List<BaseSampleSystem>();
    states = new List<bool>();

    AddSystem<SampleSimpleSystem>();
    AddSystem<SampleCustomSystem>();
    AddSystem<SampleSpawnerSystem>();
    AddSystem<SampleParentTransformSystem>();
    AddSystem<SampleGroupedPositionSystem>();
    AddSystem<SampleGroupedRotationSystem>();
  }

  private void AddSystem<T>()where T : BaseSampleSystem {
    systems.Add(World.Active.GetOrCreateManager<T>());
    states.Add(systems[systems.Count - 1].Enabled);
  }

  protected override JobHandle OnUpdate(JobHandle deps) {
    var curState = false;
    for (int i = 0; i < systems.Count; i++) {
      curState = systems[i].Enabled;
      if (curState != states[i]) {
        states[i] = curState;

        if (curState == true) {
          systems[i].Init(em);
        } else {
          systems[i].Dest(em);
        }
      }
    }

    return deps;
  }
}
