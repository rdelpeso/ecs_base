using UnityEngine;

public static class SampleResources {
  public static Mesh CubeMesh() {
    var mesh = new Mesh();

    mesh.vertices = new Vector3[] {
      new Vector3(0, 0, 0),
      new Vector3(1, 0, 0),
      new Vector3(1, 1, 0),
      new Vector3(0, 1, 0),
      new Vector3(0, 0, 1),
      new Vector3(1, 0, 1),
      new Vector3(1, 1, 1),
      new Vector3(0, 1, 1),
    };

    mesh.triangles = new int[] {
      // Front
      0,
      2,
      1,
      0,
      3,
      2,

      // Back
      4,
      5,
      6,
      4,
      6,
      7,

      // Top
      3,
      6,
      2,
      3,
      7,
      6,

      // Bottom
      0,
      1,
      5,
      0,
      5,
      4,

      // Left
      0,
      4,
      7,
      0,
      7,
      3,

      // Right
      1,
      6,
      5,
      1,
      2,
      6
    };

    mesh.RecalculateBounds();
    mesh.RecalculateNormals();
    mesh.RecalculateTangents();

    return mesh;
  }

  public static Material Material() => new Material(Shader.Find("Standard"));
}
