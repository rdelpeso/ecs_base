using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

public class SampleBarrier : BarrierSystem { }
public struct BaseSampleSystemComponent : IComponentData { }

public abstract class BaseSampleSystem : JobComponentSystem {
  protected EntityManager em;
  protected MeshInstanceRenderer mir;
  protected ComponentGroup SystemGroup;

  [Inject] protected SampleBarrier barrier;

  // Disabled by default
  protected override void OnCreateManager() {
    Enabled = false;

    em = World.Active.GetExistingManager<EntityManager>();

    mir = new MeshInstanceRenderer {
      mesh = SampleResources.CubeMesh(),
      material = SampleResources.Material(),
    };

    SystemGroup = GetComponentGroup(ComponentType.ReadOnly(typeof(BaseSampleSystemComponent)));
  }

  /* Setup sample data */
  public virtual EntityCommandBuffer Init(EntityManager em, int entityCount = 1) {
    Enabled = true;

    var buffer = barrier.CreateCommandBuffer();
    for (int i = 0; i < entityCount; i++) {
      BufferCubeEntity(buffer);
    }
    return buffer;
  }

  /* Cleanup sample data */
  public virtual EntityCommandBuffer Dest(EntityManager em) {
    var entities = SystemGroup.GetEntityArray();
    var len = SystemGroup.CalculateLength();
    var buffer = barrier.CreateCommandBuffer();
    if (len > 0) {
      for (int i = 0; i < len; i++) {
        buffer.DestroyEntity(entities[i]);
      }
    }

    Enabled = false;

    return buffer;
  }

  protected Entity GetCubeEntity() {
    var entity = em.CreateEntity();
    em.AddComponentData(entity, new BaseSampleSystemComponent());
    em.AddComponentData(entity, new Position());
    em.AddComponentData(entity, new Rotation());
    em.AddSharedComponentData(entity, mir);

    return entity;
  }

  protected void BufferCubeEntity(EntityCommandBuffer buffer) {
    buffer.CreateEntity();
    buffer.AddComponent(new BaseSampleSystemComponent());
    buffer.AddComponent(new Position());
    buffer.AddComponent(new Rotation());
    buffer.AddSharedComponent(mir);
  }
}
