using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(UnityEngine.Experimental.PlayerLoop.FixedUpdate))]
public class SampleGrouped { }

[UpdateInGroup(typeof(SampleGrouped))]
public class SampleGroupedPositionSystem : BaseSampleSystem {
  protected override JobHandle OnUpdate(JobHandle deps) {
    return new SampleGroupedPositionJob { dt = Time.deltaTime }.Schedule(this, deps);
  }

  [BurstCompile]
  struct SampleGroupedPositionJob : IJobProcessComponentData<Rotation, Position, BaseSampleSystemComponent> {
    [ReadOnly] public float dt;

    public void Execute([ReadOnly] ref Rotation rotation, ref Position position, [ReadOnly] ref BaseSampleSystemComponent cmp) {
      position.Value = position.Value + dt * math.forward(rotation.Value);
    }
  }
}

[UpdateInGroup(typeof(SampleGrouped))]
public class SampleGroupedRotationSystem : BaseSampleSystem {
  protected override JobHandle OnUpdate(JobHandle deps) {
    return new SampleGroupedRotationJob { dt = Time.deltaTime }.Schedule(this, deps);
  }

  [BurstCompile]
  struct SampleGroupedRotationJob : IJobProcessComponentData<Rotation, BaseSampleSystemComponent> {
    [ReadOnly] public float dt;

    public void Execute(ref Rotation rotation, [ReadOnly] ref BaseSampleSystemComponent cmp) {
      rotation.Value = math.mul(math.normalizesafe(rotation.Value), quaternion.AxisAngle(math.up(), 0.5f * dt));
    }
  }
}
