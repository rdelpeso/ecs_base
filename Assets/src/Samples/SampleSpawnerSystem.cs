using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

[AlwaysUpdateSystem]
public class SampleSpawnerSystem : BaseSampleSystem {
  // Components
  public struct SampleSpawnerComponent : IComponentData { public int Value; }
  public struct SampleSpawnerSpawnComponent : IComponentData { }

  // Required dependencies
  private ComponentGroup AllGroup;
  private ComponentGroup SpawnerGroup;
  private ComponentGroup SpawnerSpawnGroup;

  public override EntityCommandBuffer Init(EntityManager em, int entityCount = 1) {
    // This is pretty much the shape used in the BaseSampleSystem!
    AllGroup = GetComponentGroup(ComponentType.ReadOnly(typeof(BaseSampleSystemComponent)));
    SpawnerGroup = GetComponentGroup(ComponentType.ReadOnly(typeof(SampleSpawnerComponent)));
    SpawnerSpawnGroup = GetComponentGroup(ComponentType.ReadOnly(typeof(SampleSpawnerSpawnComponent)));

    var buffer = barrier.CreateCommandBuffer();
    buffer.CreateEntity();
    buffer.AddComponent(new SampleSpawnerComponent { Value = 10 });
    buffer.AddComponent(new BaseSampleSystemComponent());

    return buffer;
  }

  public override EntityCommandBuffer Dest(EntityManager em) {
    // This is pretty much the shape used in the BaseSampleSystem!
    var entities = AllGroup.GetEntityArray();
    var len = AllGroup.CalculateLength();
    var buffer = barrier.CreateCommandBuffer();
    if (len > 0) {
      for (int i = 0; i < len; i++) {
        buffer.DestroyEntity(entities[i]);
      }
    }

    Enabled = false;

    return buffer;
  }

  // Update
  protected override JobHandle OnUpdate(JobHandle deps) {
    // If there is no spawner, there is nothing to do!
    if (SpawnerGroup == null || SpawnerGroup.CalculateLength() == 0) {
      return deps;
    }

    var spawner = SpawnerGroup.GetComponentDataArray<SampleSpawnerComponent>();
    var len = SpawnerSpawnGroup.CalculateLength();

    // Only spawn things if we are not above the max
    if (len >= spawner[0].Value) {
      return deps;
    }

    var position = new float3(
      UnityEngine.Random.Range(-2f, 2f),
      UnityEngine.Random.Range(-2f, 2f),
      UnityEngine.Random.Range(-2f, 2f)
    );
    var rotation = quaternion.identity;

    // Don't do stuff on the same frame, just queue it! Also notice we are dealing one by one!
    var buffer = barrier.CreateCommandBuffer();
    buffer.CreateEntity();
    buffer.AddComponent(new BaseSampleSystemComponent());
    buffer.AddComponent(new SampleSpawnerSpawnComponent { });
    buffer.AddComponent(new Position { Value = position });
    buffer.AddComponent(new Rotation { Value = rotation });
    buffer.AddSharedComponent(mir); // We use the Base Class MIR here, so it is shared.

    return deps;
  }
}
