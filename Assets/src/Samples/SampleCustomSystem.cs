﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class SampleCustomSystem : BaseSampleSystem {
  private ComponentGroup MoveGroup;

  [BurstCompile]
  struct SampleCustomJob : IJobParallelFor {
    [ReadOnly] public float dt;
    [ReadOnly] public ComponentDataArray<Rotation> rotations;
    public ComponentDataArray<Position> positions;

    public void Execute(int index) {
      var position = positions[index];
      var rotation = rotations[index];

      position.Value = position.Value + dt * math.forward(rotation.Value);

      positions[index] = position;
    }
  }

  protected override void OnCreateManager() {
    base.OnCreateManager();

    MoveGroup = GetComponentGroup(ComponentType.ReadOnly(typeof(Rotation)), typeof(Position), ComponentType.ReadOnly(typeof(BaseSampleSystemComponent)));
  }

  protected override JobHandle OnUpdate(JobHandle inputDeps) {
    var positions = MoveGroup.GetComponentDataArray<Position>();
    var rotations = MoveGroup.GetComponentDataArray<Rotation>();

    var job = new SampleCustomJob {
      dt = Time.deltaTime,
      positions = positions,
      rotations = rotations
    };

    return job.Schedule(positions.Length, 64, inputDeps);
  }
}
