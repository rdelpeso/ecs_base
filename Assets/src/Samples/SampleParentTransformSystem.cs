using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class SampleParentTransformSystem : BaseSampleSystem {
  struct SpeedComponent : IComponentData { public float Value; };

  public override EntityCommandBuffer Init(EntityManager em, int entityCount = 0) {
    var buffer = base.Init(em, 0);

    var parent = GetCubeEntity();
    em.AddComponentData(parent, new SpeedComponent { Value = 1f });
    var child = GetCubeEntity();
    em.AddComponentData(child, new SpeedComponent { Value = 2f });

    // Here is where the magic happens. This entity will be removed auto-magically by the TransformSystem after linking
    // the Child to the Parents matrix. So that transforms are done locally rather than globally.
    // https://github.com/Unity-Technologies/EntityComponentSystemSamples/blob/master/Documentation/reference/transform_system.md#attaching-transformations
    var link = em.CreateEntity();
    em.AddComponentData(link, new Attach { Parent = parent, Child = child });

    return buffer;
  }

  protected override JobHandle OnUpdate(JobHandle deps) =>
    new SampleParentTransformJob { dt = Time.deltaTime, tss = Time.realtimeSinceStartup }.Schedule(this, deps);

  [BurstCompile]
  struct SampleParentTransformJob : IJobProcessComponentData<Rotation, BaseSampleSystemComponent, SpeedComponent, Position> {
    [ReadOnly] public float dt;
    [ReadOnly] public float tss;

    public void Execute([ReadOnly] ref Rotation rotation, [ReadOnly] ref BaseSampleSystemComponent cmp, [ReadOnly] ref SpeedComponent speed, ref Position position) {
      position.Value = new float3(
        5 * math.sin(tss * speed.Value),
        0f,
        5 * math.cos(tss * speed.Value)
      );
      position.Value = position.Value + dt * math.forward(rotation.Value);
    }
  }
}
