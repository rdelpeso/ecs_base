﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class SampleSimpleSystem : BaseSampleSystem {
  protected override JobHandle OnUpdate(JobHandle deps) {
    return new SampleSimpleJob { dt = Time.deltaTime }.Schedule(this, deps);
  }

  [BurstCompile]
  struct SampleSimpleJob : IJobProcessComponentData<Rotation, Position, BaseSampleSystemComponent> {
    [ReadOnly] public float dt;

    public void Execute([ReadOnly] ref Rotation rotation, ref Position position, [ReadOnly] ref BaseSampleSystemComponent cmp) {
      position.Value = position.Value + dt * math.forward(rotation.Value);
    }
  }
}
